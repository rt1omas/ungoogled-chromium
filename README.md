# ungoogled-chromium

This repository is a fork of [ungoogled-chromium-debian](https://github.com/ungoogled-software/ungoogled-chromium-debian) with slightly modified files in order to upload to the Debian Archives.

## Introduction
Without signing in to a Google Account, Chromium does pretty well in terms of security and privacy. However, Chromium still has some dependency on Google web services and binaries. In addition, Google designed Chromium to be easy and intuitive for users, which means they compromise on transparency and control of internal operations.

ungoogled-chromium addresses these issues in the following ways:

1. Remove all remaining background requests to any web services while building and running the browser
2. Remove all code specific to Google web services
3. Remove all uses of pre-made binaries from the source code, and replace them with user-provided alternatives when possible.
4. Disable features that inhibit control and transparency, and add or modify features that promote them (these changes will almost always require manual activation or enabling).

You can read more at: [ungoogled-chromium](https://github.com/Eloston/ungoogled-chromium)

## Installation Guide

**NOTE**: The packages are essentially identical in structure to Debian's `chromium` packages. **As a result, they cannot be installed simultaneously with the distro-provided Chromium package.**

At minimum, you will need to install the `ungoogled-chromium` and `ungoogled-chromium-common` packages.

* If you have the `.deb` packages:

	```sh
	# dpkg -i ungoogled-chromium_*.deb ungoogled-chromium-common_*.deb
	```
The other packages are as follows:

* `*-driver`: [ChomeDriver](http://chromedriver.chromium.org/)
* `*-l10n`: Localization package for the browser UI.
* `*-sandbox`: [`SUID` Sandbox](https://chromium.googlesource.com/chromium/src/+/lkgr/docs/linux_suid_sandbox.md). This is only necessary if you do not have user namespaces enabled (i.e. kernel parameter `kernel.unprivileged_userns_clone`)
* `*-shell`: Contains `content_shell`. Mainly for browser development/testing; search [the Chromium docs](https://chromium.googlesource.com/chromium/src/+/lkgr/docs/) for more details.
* `*-dbgsym*`: Debug symbols for the associated package.

## Build Dependencies (These do not have to be installed manually, they are just listed here.)
- debhelper (>= 11),
- clang-10,
- clang-format-10,
- lld-10,
- llvm-10-dev,
- python2,
- python3,
- python3-debian,
- pkg-config,
- ninja-build,
- python-jinja2,
- python-pkg-resources,
- ca-certificates,
- gsettings-desktop-schemas-dev,
- wget,
- flex,
- yasm,
- xvfb,
- wdiff,
- gperf,
- bison,
- nodejs,
- valgrind,
- xz-utils,
- x11-apps,
- xfonts-base,
- libx11-xcb-dev,
- libgl1-mesa-dev,
- libglu1-mesa-dev,
- libegl1-mesa-dev,
- libgles2-mesa-dev,
- mesa-common-dev,
- libxt-dev,
- libre2-dev,
- libgbm-dev,
- libxss-dev,
- libelf-dev,
- libvpx-dev (>= 1.7.0),
- libpci-dev,
- libcap-dev,
- libdrm-dev,
- libicu-dev,
- libffi-dev,
- libkrb5-dev,
- libexif-dev,
- libflac-dev,
- libudev-dev,
- libopus-dev,
- libwebp-dev,
- libxtst-dev,
- libjpeg-dev,
- libxml2-dev,
- libgtk-3-dev,
- libxslt1-dev,
- liblcms2-dev,
- libpulse-dev,
- libpam0g-dev,
- libsnappy-dev,
- libavutil-dev,
- libavcodec-dev (>= 7),
- libavformat-dev,
- libglib2.0-dev,
- libasound2-dev,
- libjsoncpp-dev,
- libspeechd-dev (>= 0.8.3),
- libminizip-dev,
- libhunspell-dev,
- libharfbuzz-dev (>= 2.1.1),
- libxcb-dri3-dev,
- libusb-1.0-0-dev,
- libopenjp2-7-dev,
- libmodpbase64-dev,
- libnss3-dev (>= 3.12.3),
- libnspr4-dev (>= 2:4.9),
- libcups2-dev (>= 1.5.0),
- libevent-dev (>= 1.4.13),
- libgcrypt20-dev,
- libva-dev,
- fonts-ipafont-gothic,
- fonts-ipafont-mincho

## Build 

Information about building:
- Warnings including "File not found" are normal
- It is normal for builds throughout all the stages to take a while

### Building .deb files

```sh
# Install essential requirements
sudo apt install git python3 packaging-dev equivs

# Clone this repository

# Create a build folder 
mkdir -p build/src
cp -r ungoogled-chromium/debian build/src/
cd build/src

# Install remaining requirements to build Chromium
sudo mk-build-deps -i debian/control
rm ungoogled-chromium-build-deps_*

# Download and unpack Chromium sources
./debian/scripts/setup local-src

# Start building (This takes a long time)
dpkg-buildpackage -b -uc
```

#### Restarting the build

If the build aborts during the last command, it can be restarted with this command:

```sh
dpkg-buildpackage -b -uc -nc
```

If it still fails, then try this command (this will clear any intermediate build outputs):

```sh
dpkg-buildpackage -b -uc
```

If all else fails, delete the entire build tree and start again.

### Building source package

*Build Debian source package (i.e. `.dsc`, `.orig.tar.xz`, and `.debian.tar.xz`). This is useful for online build services like Launchpad and openSUSE Build Service.*

```sh
# Install essential requirements
sudo apt install git python3 packaging-dev equivs

# Clone this repo

# Create a build folder 
mkdir -p build/src
cp -r ungoogled-chromium/debian build/src/
cd build/src

# Download and unpack Chromium sources (this will take some time)
./debian/scripts/setup local-src

# Create a source tarball suitable for use with a Debian source package (this will take some time)
./debian/scripts/setup orig-source

# Create the Debian source package (this will take some time)
debuild -S -sa -d
```

(`PACKAGE_TYPE_HERE` is the same as above)

Source package files will appear under `build/`

## Differences between Debian's Chromium

There are a few differences with Debian's Chromium:

* Modified default CLI flags and preferences; see `debian/etc/default-flags` and `debian/etc/master_preferences`
* Re-enable `chrome://tracing` and DevTool's Performance tab
* Package names are prefixed with `ungoogled-`, and will conflict Debian's Chromium packages.
* Enable more FFMpeg codecs by default
* Use tcmalloc (Chromium default) instead of the system's memory allocator
